## How To Use
1. Download atau clone aplikasi dengan menggunakan perintah berikut.
```
git clone https://gitlab.com/dwikyramadhanh-my-repository/php/basic-crud-php
```
2. Letakkan folder aplikasi pada web server. <br /> 
<b>Note: </b> jika menggunakan XAMPP, bisa diletakkan di folder htdocs.
3. Buat database dengan nama <b>basic-crud-php</b> pada DBMS Anda. <br />
<b>Note: </b> jika menggunakan XAMPP, bisa menggunakan phpmyadmin untuk membuat databasenya.
4. Lalu import database <b>employees</b> yang terdapat pada folder aplikasi.
5. Jalankan aplikasi dengan menggunakan perintah berikut pada web browser.
```
localhost/basic-crud-php
``` 
