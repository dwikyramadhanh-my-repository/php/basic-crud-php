<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <title>Add Employee Data</title>
</head>

<body>
  <!-- navigation -->
  <nav class="navbar bg-dark">
    <div class="container">
      <a class="navbar-brand" href="index.php">
        <span class="text-white fw-bold">GREENK</span>
      </a>
    </div>
  </nav>
  <!-- content -->
  <div class="container mt-4">
    <div class="card border-0 shadow">
      <div class="card-header">
        <h4 class="text-start text-primary">Add data employee</h4>
      </div>
      <div class="card-body">
        <form action="insert.php" method="POST">
          <!-- name -->
          <div class="mb-3">
            <label for="name" class="form-label">Name</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Name..." autofocus required>
          </div>

          <!-- address -->
          <div class="mb-3">
            <label for="address" class="form-label">Address</label>
            <textarea name="address" id="address" cols="30" rows="10" class="form-control" placeholder="Address..."
              required></textarea>
          </div>

          <!-- age -->
          <div class="mb-3">
            <label for="age" class="form-label">Age</label>
            <input type="number" name="age" class="form-control" id="age" placeholder="Age..." required>
          </div>

          <!-- gender -->
          <div class="mb-3">
            <label for="gender" class="form-label">Gender</label>
            <select name="gender" class="form-select" required>
              <option value="Man">Man</option>
              <option value="Woman">Woman</option>
            </select>
          </div>

          <!-- button submit -->
          <button type="submit" class="btn btn-md btn-primary">Save</button>
        </form>
      </div>
    </div>
  </div>
  <script src="assets/js/bootstrap.bundle.min.js"></script>
</body>

</html>