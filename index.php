<?php
include("connection.php");
$query = mysqli_query($connect, "SELECT * FROM employees");
$results = mysqli_fetch_all($query, MYSQLI_ASSOC);
/* 
    Mysqli_fetch_all digunakan untuk mengambil data atau semua baris hasil dan mengembalikan 
    set hasil sebagai array asosiatif, array numerik, atau keduanya. MYSQLI_ASSOC digunakan agar key array dapat diakses dengan nama
*/
// print_r($results);
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <title>Employee Data</title>
</head>

<body>
  <!-- navigation -->
  <nav class="navbar bg-dark">
    <div class="container">
      <a class="navbar-brand" href="index.php">
        <span class="text-white fw-bold">GREENK</span>
      </a>
    </div>
  </nav>
  <!-- content -->
  <div class="container mt-4">
    <div class="card border-0 shadow">
      <div class="card-body">
        <div class="row">
          <!-- Search -->
          <div class="col-md-10">
            <form action="search.php" method="GET" id="search">
              <div class="mb-3">
                <div class="input-group mb-3">
                  <input type="search" name="keyword" placeholder="Search..." class="form-control">
                  <button type="submit" class="input-group-text"><svg xmlns="http://www.w3.org/2000/svg" width="16"
                      height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                      <path
                        d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                    </svg>
                  </button>
                  <!-- <input type="submit" value="Search" class="input-group-text"> -->
                </div>
              </div>
            </form>
          </div>
          <!-- add data -->
          <div class="col-md-2 d-flex justify-content-end">
            <div class="mb-3">
              <a href="add.php" class="btn btn-md btn-primary">Add Data</a>
            </div>
          </div>
        </div>

        <!-- table employee -->
        <div class="table-responsive">
          <table class="table table-hover">
            <caption>Employee Data Table</caption>
            <thead class="table-dark">
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Address</th>
                <th>Age</th>
                <th>Gender</th>
                <th style="width: 20%;">Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php $no = 1 ?>
              <?php  if (count($results) !== 0) : ?>
              <?php foreach ($results as $result): ?>
              <tr>
                <td><?php echo $no++; ?></td>
                <td><?php echo $result["name"]; ?></td>
                <td><?php echo $result["address"]; ?></td>
                <td><?php echo $result["age"]; ?></td>
                <td><?php echo $result["gender"]; ?></td>
                <td>
                  <a href="edit.php?id=<?php echo $result["id"]; ?>" id="edit"
                    class="btn btn-md btn-warning text-white mb-2">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                      class="bi bi-pencil" viewBox="0 0 16 16">
                      <path
                        d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z" />
                    </svg>
                  </a>
                  <a href="delete.php?id=<?php echo $result["id"]; ?>" id="delete" class="btn btn-md btn-danger mb-2"
                    onclick="return confirm('Anda yakin ingin menghapus data ini?');">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                      class="bi bi-trash" viewBox="0 0 16 16">
                      <path
                        d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                      <path fill-rule="evenodd"
                        d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                    </svg>
                  </a>
                </td>
              </tr>
              <?php endforeach; ?>
              <?php elseif(count($results) === 0):?>
              <tr>
                <td colspan="6" class="text-center fw-bold text-danger">No data available</td>
              </tr>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <script src="assets/js/bootstrap.bundle.min.js"></script>
</body>

</html>